const Exercise = require('../models/exercise.model');
const router = require('express').Router();

router.route('/').get((req, res) => {
  Exercise.find()
    .then((exercises) => res.json(exercises))
    .catch((err) => res.status(400).json('Error: ', err));
});

router.route('/add').post((req, res) => {
  const { username, description, duration, date } = req.body;

  const exercise = new Exercise({ username, description, duration, date });
  exercise
    .save()
    .then((exercise) => res.json(exercise))
    .catch((err) => res.status(400).json('Error: ' + err));
});

router.route('/:id').get((req, res) => {
  Exercise.findById(req.params.id)
    .then((exercise) => res.json(exercise))
    .catch((err) => res.status(400).json('Error: ' + err));
});

router.route('/:id').delete((req, res) => {
  Exercise.findByIdAndDelete(req.params.id)
    .then((exercise) => res.json(exercise))
    .catch((err) => res.status(400).json('Error: ' + err));
});

router.route('/update/:id').post((req, res) => {
    Exercise.findById(req.params.id)
        .then((exercise) => {
            exercise.username = req.body.username;
            exercise.description = req.body.description;
            exercise.duration = Number(req.body.duration);
            exercise.date = Date(req.body.date);

            return exercise.save();
        })
        .then((exercise) => res.json(exercise))
        .catch((err) => res.status(400).json('Error: ' + err));
});

module.exports = router;
