const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const exercisesRouter = require('./routes/exercises');
const usersRouter = require('./routes/users');

// configures so we can have env variables in .env file
require('dotenv').config();

// Setting up express app
const app = express();
// Using port from env or default
const port = process.env.PORT || 5000;

// App cors middlewear 
app.use(cors());
// Telling app to use JSON
app.use(express.json());

// Getting Mongo Atlas uri from env
const uri = process.env.ATLAS_URI;
// Connecting to DB - useNewUrlParser: mongo db nodejs driver rewrote the tool and needs to set flags to get new features.
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true });
const connection = mongoose.connection;
connection.once('open', () => {
    console.log('MongoDB connection established successfully!');
})

app.use('/exercises', exercisesRouter);
app.use('/users', usersRouter)

app.listen(port, ()=> {
    console.log(`Server is running  in port: ${port} `);
});