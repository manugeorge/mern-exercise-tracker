import React, { Component } from 'react';
import { Card, Button, Container, Row, Col } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import './edit-exercise.component.css';

export default class EditExercise extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      description: '',
      duration: 0,
      date: new Date(),
      _id: undefined,
      users: [],
    };
    this.setLatestExercise = this.setLatestExercise.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onDateChange = this.onDateChange.bind(this);
    this.setUsers = this.setUsers.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  setUsers(users) {
    console.log('users:', users);
    this.setState({
      users,
    });
  }

  setLatestExercise(exercise) {
    console.log('exercise :', exercise);
    this.setState({
      username: exercise.username,
      description: exercise.description,
      duration: exercise.duration,
      date: new Date(exercise.date),
      _id: exercise._id,
    });
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  onDateChange(date) {
    this.setState({
      date,
    });
  }

  componentDidMount() {
    this.fetchUsers()
      .then((resp) => resp.json())
      .then(this.setUsers)
      .catch((err) => console.log('Error: ', err))
      .then(() => this.fetchExercise())
      .then((resp) => resp.json())
      .then(this.setLatestExercise)
      .catch((err) => console.log('Error: ', err));
  }

  fetchUsers() {
    return fetch(`http://localhost:5000/users`);
  }

  fetchExercise() {
    if (this.props.match.params.id) {
      return fetch(
        `http://localhost:5000/exercises/${this.props.match.params.id}`
      );
    } else {
      throw Error('No id from params');
    }
  }

  onSubmit(e) {
    e.preventDefault();
    const exercise = {
      _id: this.state._id,
      username: this.state.username,
      description: this.state.description,
      duration: this.state.duration,
      date: this.state.date,
    };
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(exercise),
    };

    if (this.state._id !== undefined) {
      this.updateExercise(requestOptions);
    } else {
      this.addExercise(requestOptions);
    }
  }

  updateExercise(requestOptions) {
    fetch(
      `http://localhost:5000/exercises/update/${this.state._id}`,
      requestOptions
    )
      .then((resp) => resp.json())
      .then((data) => {
        console.log(data);
        this.props.history.push('/');
      })
      .catch((err) => console.log('Error: ', err));
  }

  addExercise(requestOptions) {
    fetch(`http://localhost:5000/exercises/add`, requestOptions)
      .then((resp) => resp.json())
      .then((data) => {
        console.log(data);
        this.props.history.push('/');
      })
      .catch((err) => console.log('Error: ', err));
  }

  render() {
    return (
      <Card className="card">
        <Card.Header>
          <h3>Create / Edit Exercise</h3>
        </Card.Header>
        <form onSubmit={this.onSubmit}>
          <Container>
            <Row>
              <Col xs='4'>
                <label>User : </label>
              </Col>
              <Col md='auto'>
                <select
                className='formItem'
                  name='username'
                  value={this.state.username}
                  onChange={this.onChange}
                  md='auto'
                >
                  {this.state.users.map((user) => {
                    return (
                      <option key={user._id} value={user.username}>
                        {user.username}
                      </option>
                    );
                  })}
                </select>
              </Col>
            </Row>
            <Row>
              <Col xs='4'>
                <label>Description : </label>
              </Col>
              <Col md='auto'>
                <input
                  type='text'
                  name='description'
                  value={this.state.description}
                  onChange={this.onChange}
                />
              </Col>
            </Row>
            <Row>
              <Col xs='4'>
                <label>Duration : </label>
              </Col>
              <Col md='auto'>
                <input
                  type='text'
                  name='duration'
                  value={this.state.duration}
                  onChange={this.onChange}
                />
              </Col>
            </Row>
            <Row>
              <Col xs='4'>
                <label>Date : </label>
              </Col>
              <Col md='auto'>
                <DatePicker
                  selected={this.state.date}
                  onChange={this.onDateChange}
                />
              </Col>
            </Row>
          </Container>

          <Button className='submit' variant='primary' type='submit'>
            Submit
          </Button>
        </form>
      </Card>
    );
  }
}
