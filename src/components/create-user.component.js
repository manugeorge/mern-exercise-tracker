import React, { Component } from 'react';
import { Button } from 'react-bootstrap';

export default class CreateUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onChange(e) {
    this.setState({
      username: e.target.value,
    });
  }
  onSubmit(e) {
    e.preventDefault();
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username: this.state.username })
    };
    fetch(`http://localhost:5000/users/add`, requestOptions)
        .then(resp => {
            this.setState({ username: '' });
            alert('User created!');
        })
        .catch(error => console.log('Error: ', error));
  }
  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <input
          type='text'
          placeholder='username'
          value={this.state.username}
          onChange={this.onChange}
        ></input>
        <Button variant='primary' type='submit'>
          Submit
        </Button>
      </form>
    );
  }
}
