import React from 'react';
import { Card, Button, Container, Row, Col } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

export function ExerciseListItem(props) {
  const history = useHistory();
  function editExercise() {
    console.log('edit : ', props.exercise._id);
    history.push(`/edit/${props.exercise._id}`);
  }

  function onDelete() {
    props.onDelete(props.exercise._id);
  }

  return (
    <Card style={{ width: '25rem', margin: 'auto' }}>
      <Card.Body>
        <Card.Title>{props.exercise.description}</Card.Title>
        <Card.Text>{`Duration: ${props.exercise.duration} mins`}</Card.Text>
        <Card.Text>{`User: ${props.exercise.username}`}</Card.Text>
        <Card.Text>{`Date: ${props.exercise.date}`}</Card.Text>
        <Container>
          <Row className='justify-content-md-center'>
            <Col>
              <Button variant='primary' block onClick={editExercise}>
                Edit
              </Button>
            </Col>
            <Col>
              <Button variant='danger' block onClick={onDelete}>
                Delete
              </Button>
            </Col>
          </Row>
        </Container>
      </Card.Body>
    </Card>
  );
}
