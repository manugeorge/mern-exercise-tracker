import React, { Component } from 'react';
import { ExerciseListItem } from './exercise-list-item.component';

export default class ExercisesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      exercisesList: [],
    };
    this.onNewList = this.onNewList.bind(this);
    this.onDeleteExercise = this.onDeleteExercise.bind(this);
  }

  onNewList(list) {
    this.setState({
      exercisesList: list,
    });
  }

  onDeleteExercise(id) {
    const requestOptions = {
      method: 'Delete',
      headers: { 'Content-Type': 'application/json' }
    }
    fetch(`http://localhost:5000/exercises/${id}`, requestOptions)
      .then(resp => resp.json())
      .then(resp => {
        console.log(resp);
        const newList = this.state.exercisesList.filter(exercise => exercise._id !== id)
        this.setState({
          exercisesList: newList
        })
      })
      .catch(err => console.log('Error: ', err));
  }

  componentDidMount() {
    fetch('http://localhost:5000/exercises')
      .then((resp) => resp.json())
      .then((resp) => this.onNewList(resp))
      .catch(console.log);
  }
  render() {
    const listItems = this.state.exercisesList.map((exercise) => {
      return (
        <ExerciseListItem
          key={exercise._id}
          exercise={exercise}
          onDelete={this.onDeleteExercise}
        />
      );
    });
    return <div>{listItems}</div>;
  }
}
