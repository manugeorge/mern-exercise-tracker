import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import NavBar from './components/navbar.component';
import EditExercise from './components/edit-exercise.component';
import CreateUser from './components/create-user.component';
import ExercisesList from './components/exercises-list.component';

function App() {
  return (
    <div className="App">
      <Router>
        <NavBar/>
        <br />
        <Switch>
          <Route path="/" exact component={ExercisesList}/>
          <Route path="/edit/:id" exact component={EditExercise}/>
          <Route path="/create" exact component={EditExercise}/>
          <Route path="/user" exact component={CreateUser}/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
